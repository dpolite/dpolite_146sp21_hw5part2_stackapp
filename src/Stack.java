
import java.util.LinkedList;

/**
 *
 * @author dpolite@email.uscb.edu
 */
// 2. Create class called Stack that specifies type variable for generics
public class Stack<E> {

    // 3. declare LinkedList to hold stack, implement push,pop, peak, and size
    //    methods
    LinkedList<E> stackList = new LinkedList();

    // create push method that adds element to top of stack
    public void push(E element) {
        stackList.addFirst(element);
    } // end method push

    // create pop method that returns 
    // the element at top of stack and removes it
    public E pop() {
        return stackList.removeFirst();
    } // end method pop
    
    // create peek method that returns method at top of stack without removing it
    public E peek(){
        return stackList.peekFirst();
    } // end method peek
    
    // create method that returns the size of the stack
    public int size(){
        return stackList.size();
    } // end method size
    
} // end class Stack<E> 