public class StackApp {

    public static void main(String[] args) {
        // 4. rename class to StackApp and create a generic stack that stores String
        //    objects
        Stack<String> myStack = new Stack();
        
        // 4a. declare a 1d array to push values to the stack
        String[] myArray = {"Apples","Oranges","Bananas"};
        
        // 5. Use enhanced for loop to push array String elements to the stack
        //    and display its values and size
        for(String s: myArray){
            myStack.push(s);
            System.out.println("Push: " + myStack.peek());
        } // end enhanced for loop
        System.out.println("This stack contains " + myStack.size() + " items\n");
        
        // 6. use the peek method and display the first item and display
        //    the size of the Stack
        System.out.println("Peek: " + myStack.peek());
        System.out.println("This stack contains " + myStack.size() + " items\n");
        
        // 7. write a while loop that pops the Stack and displays its value
        //    until it's empty and display the Stack size after
        while(myStack.size() > 0){
            System.out.println("Pop: " + myStack.pop());
        } // end while loop
        
        System.out.println("This stack contains " + myStack.size() + " items");
    }
}
